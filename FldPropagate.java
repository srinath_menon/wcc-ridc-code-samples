import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;
import oracle.stellent.ridc.model.DataBinder;
import oracle.stellent.ridc.model.DataObject;
import oracle.stellent.ridc.model.DataResultSet;

/*
 * @author Srinath Menon
 *
 * Sample code for Propagating the metadata value for Framework Folder on Content Server using RIDC.
 */

public class FldPropagate{

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Create a new IdcClientManager
		IdcClientManager manager = new IdcClientManager ();
		try{
			// Create a new IdcClient Connection using idc protocol (i.e. socket connection to Content Server)
			IdcClient idcClient = manager.createClient ("idc://<ucm host name/ip>:<intradocserver port>");

                        //for using the web connection - start          
                                // IdcClient idcClient = manager.createClient("http://<ucmhostname>:<web port>/cs/idcplg");
                                // IdcContext userContext = new IdcContext("<user id>", "<password as set>");          
                        //for using web connection - end
                                  
			// Create new context using the required user
			IdcContext userContext = new IdcContext ("<user id>");
                        
			// Create an HdaBinderSerializer; this is not necessary, but it allows us to serialize the request and response data binders
			HdaBinderSerializer serializer = new HdaBinderSerializer ("UTF-8", idcClient.getDataFactory ());
			
			// Databinder for initial request
			   DataBinder dataBinder = idcClient.createBinder();
               dataBinder.putLocal("IdcService", "FLD_PROPAGATE");
               dataBinder.putLocal("fFolderGUID","<GUID>"); //Folder GUID for which propagation has to be implemented
               dataBinder.putLocal("xComments:isSelected","1"); //xMetadata:isSelected will select which field from folder should be propagated
               dataBinder.putLocal("xComments","FromParent"); //Value of the metadata
               
			   //Optional Parameter 
			   dataBinder.putLocal("propagateToFoldersOnly","1"); // If this is set to 1 (by default is set to 0) , then the propagation will be only for the child folders.
			   dataBinder.putLocal("isForceRecursive","1"); //If this is set to 1 (by default is set to 0) , then the propagation is forced on the items
			   
			   
			   
            // Write the data binder for the request to stdout
            serializer.serializeBinder (System.out, dataBinder);
            // Send the request to Content Server
            ServiceResponse response = idcClient.sendRequest(userContext,dataBinder);
            // Get the data binder for the response from Content Server
            DataBinder responseData = response.getResponseAsBinder();
            // Write the response data binder to stdout
            serializer.serializeBinder (System.out, responseData);
           

		} catch (IdcClientException ice){
			ice.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}

}
