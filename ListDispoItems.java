import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;
import java.util.*;
import java.text.*;

/*
 * @author Srinath Menon
 * 
 * This is a class used to list the content items (or records) and Record Folders which are ready for Disposition action.
 * They are listed after a Batch service is executed and can be viewed from WCC:Record - My Record Assignment - Actions - List Disposition Folders and Contents
 * Using the following Service LIST_DISPOSITION_FOLDERS_AND_CONTENT_MINE
 */

public class ListDispoItems {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

         

   	        // Create a new IdcClientManager
		IdcClientManager manager = new IdcClientManager ();
		try{
			// Create a new IdcClient Connection using idc protocol (i.e. socket connection to Content Server)
			 IdcClient idcClient = manager.createClient ("idc://<wcc:rec hostname>:<intradoc server port>");

                                 //for using the web connection - start          
                                // IdcClient idcClient = manager.createClient("http://<wcc:rec hostname>:<web port>/urm/idcplg/");
                                IdcContext userContext = new IdcContext("<user id>", "<password>");          
                                //for using web connection - end
                                  
                       
			// Create an HdaBinderSerializer; this is not necessary, but it allows us to serialize the request and response data binders
			HdaBinderSerializer serializer = new HdaBinderSerializer ("UTF-8", idcClient.getDataFactory ());
			
			// Databinder for initial request
			DataBinder dataBinder = idcClient.createBinder();
            dataBinder.putLocal("IdcService", "LIST_DISPOSITION_FOLDERS_AND_CONTENT_MINE");
            dataBinder.putLocal("dDispositionID","<dDispositionID for which the items have to be listed>");
           
            serializer.serializeBinder (System.out, dataBinder);
            // Send the request to Content Server
            ServiceResponse response = idcClient.sendRequest(userContext,dataBinder);
            // Get the data binder for the response from Content Server
            DataBinder responseData = response.getResponseAsBinder();
            // Write the response data binder to stdout
            serializer.serializeBinder (System.out, responseData);
            // Retrieve the Content Items as first result set
            DataResultSet resultSet = responseData.getResultSet("CONTENT_CHILDREN");
            // Retrieve Records Folder List if any from second result set
            DataResultSet resultSetFolder = responseData.getResultSet("FOLDER_CHILDREN");

            
            
            // Iterate over the CONTENT_CHILDREN ResultSet, retrieve Content item details
            for (DataObject dataObject : resultSet.getRows ()) {
                   
               System.out.println ("Content ID  are  : " + dataObject.get ("dID") );
               System.out.println ("Category ID is :" + dataObject.get("dCategoryID"));
               System.out.println ("Content name is :" + dataObject.get("dDocTitle"));
               }

            // Iterate over the FOLDER_CHILDREN ResultSet, retrieve properties for the Record Folders
            for (DataObject dataObject : resultSetFolder.getRows ()) {

               System.out.println ("Retention Folder ID  are  : " + dataObject.get ("dFolderID"));
               System.out.println ("Retention Folder Name :" + dataObject.get("dFolderName"));
               }

			
		} catch (IdcClientException ice){
			ice.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}

}
