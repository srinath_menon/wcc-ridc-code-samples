import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;

 /* 
 *   This class is sample code that is used to checkin a new content to Framework Folders
 *   Framework folders is part of WCC PS5 and changes to RIDC api is needed to achieve 
 *   the requirement 
 */

public class CheckintoFrameworkFolder{

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Create a new IdcClientManager
		IdcClientManager manager = new IdcClientManager ();
		try{
			// Create a new IdcClient Connection using idc protocol (i.e. socket connection to Content Server)
			IdcClient idcClient = manager.createClient ("idc://hostname:intradocserverport");

                                                         
			// Create new context using the 'sysadmin' user
			IdcContext userContext = new IdcContext ("weblogic");
                        
			// Create an HdaBinderSerializer; this is not necessary, but it allows us to serialize the request and response data binders
			HdaBinderSerializer serializer = new HdaBinderSerializer ("UTF-8", idcClient.getDataFactory ());
			
			// Databinder for checkin request
			            DataBinder dataBinder = idcClient.createBinder();
                        dataBinder.putLocal("IdcService", "CHECKIN_NEW");
                        dataBinder.putLocal("dDocTitle", "Framework Test-4");
                       //binder.putLocal("dDocName", ""); /*In case of auto content id creation , dDocName is not mandatory*/
                        dataBinder.putLocal("dDocType", "Document");  
                        dataBinder.putLocal("dSecurityGroup", "Public");
                        dataBinder.addFile("primaryFile", new File("<path to the file which is to be checked in>"));
                        dataBinder.putLocal("doFileCopy", "true");
                        dataBinder.putLocal("dDocAuthor", "weblogic");
                       // This is the key change w.r.t Framework Folders . Here we user fParentGUID as the key , where as in Folders_g this would be xCollectionID
                        dataBinder.putLocal("fParentGUID",  "4462C1F1885E40B7E3382F4421BFC2A5");
            // Write the data binder for the request to stdout
            serializer.serializeBinder (System.out, dataBinder);
            // Send the request to Content Server
            ServiceResponse response = idcClient.sendRequest(userContext,dataBinder);
            // Get the data binder for the response from Content Server
            DataBinder responseData = response.getResponseAsBinder();
            // Write the response data binder to stdout
            serializer.serializeBinder (System.out, responseData);
           
			
		} catch (IdcClientException ice){
			ice.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}

}
