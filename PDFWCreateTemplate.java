import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;
import java.util.*;
import java.text.*;

/*
 * @author Srinath Menon 
 *
 * Sample RIDC to create PDF Watermark Templates  using RIDC
 */
 
public class PDFWCreateTemplate {
	// RIDC connection information
	private static final String IDC_PROTOCOL = "idc://";
	private static final String RIDC_SERVER = "<ucm server hostname>";
	private static final String RIDC_PORT = "<intradoc server port>";
	private static IdcClientManager m_idcClientManager;
	private static IntradocClient m_idcClient;

	private static final String UTF8 = "UTF-8";

	// Service parameters
	private static final String IDC_SERVICE = "IdcService";
	private static final String SAVE_TEMPLATE = "PDFW_SAVE_NEW_TEMPLATE";
	private static final String GET_TEMPLATE = "PDFW_GET_TEMPLATE_NAMES";
	
         // User to execute service calls as
	private static final String USER = "weblogic";

	// Used for getting the Result Set for PDFW Rules - This will be used to update new data
	private static final String TEMPLATE_RESULT = "PdfwTemplates";

        // Used for Saving the New Templates 
        private static final String TEXT_TEMPLATE = "PdfwTextWatermarks";
        private static final String IMAGE_TEMPLATE = "PdfwImageWatermarks";
        private static final String ESIG_TEMPLATE = "PdfwSignatureWatermarks";  


        /* PDFW Templates  result set has different set of for Text , Image and Singature Watermarks respectively . These  parameters which are initialized here */
        public static DataResultSet.Field TEXT= new DataResultSet.Field("text");
        public static DataResultSet.Field LOCATION= new DataResultSet.Field("location");
        public static DataResultSet.Field ROTATION= new DataResultSet.Field("rotation");
        public static DataResultSet.Field ALIGNMENT= new DataResultSet.Field("alignment");
        public static DataResultSet.Field FONTNAME= new DataResultSet.Field("fontName");
        public static DataResultSet.Field FONTSIZE= new DataResultSet.Field("fontSize");
        public static DataResultSet.Field FONTWEIGHT= new DataResultSet.Field("fontWeight");
        public static DataResultSet.Field FONTCOLOR= new DataResultSet.Field("fontColor");
        public static DataResultSet.Field LAYER= new DataResultSet.Field("layer");
        public static DataResultSet.Field PAGERANGE= new DataResultSet.Field("pageRange");
        public static DataResultSet.Field PAGERANGEMODIFIER= new DataResultSet.Field("pageRangeModifier");  
        public static DataResultSet.Field X_COORD= new DataResultSet.Field("x_coord");
        public static DataResultSet.Field Y_COORD= new DataResultSet.Field("y_coord");
        public static DataResultSet.Field IMAGEID = new DataResultSet.Field("imageID");
        public static DataResultSet.Field SCALEFACTOR = new DataResultSet.Field("scaleFactor");
        public static DataResultSet.Field LABEL = new DataResultSet.Field("blockLabel");
         

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(final String[] args) {
		try {
			// Get ResultSet PdfwRulesDRS from service call PDFW_GET_RULES
			final DataResultSet templateResultSet = getPdfwTemplates();
			
                        // Create New rule based on requirement

                        DataFactory dataFactory =m_idcClient.getDataFactory();
                        final DataResultSet PdfwTextWatermarks = dataFactory.createResultSet();
                        final DataResultSet PdfwImageWatermarks = dataFactory.createResultSet();
                        final DataResultSet PdfwSignatureWatermarks = dataFactory.createResultSet();
			            savePdfwTemplates(PdfwTextWatermarks);
						
						/*uncomment this code block when requirement is to create new image watermarks*/
                        //savePdfwImageTemplates(PdfwImageWatermarks);   
						
						/*uncomment this code block when requirement is to create new signature watermarks*/
                        //savePdfwSignatureTemplates(PdfwSignatureWatermarks); 

		} catch (final IdcClientException ice){
			ice.printStackTrace(System.out);
		}
	}

	/**
	 * This method executes PDFW_GET_TEMPLATE_NAMES and returns the PdfwTemplates ResultSet.
	 * @return The PdfwRulesDRS ResultSet.
	 * @throws IdcClientException
	 * @throws IOException
         * This section is provided in case the requirement needs to show the existing set of templates on the server 
	 */

	private static DataResultSet getPdfwTemplates() throws IdcClientException {
		// DataBinder for service call
		DataBinder serviceBinder = getNewDataBinder();

		// Set service parameters - PDFW_GET_RULES
		serviceBinder.putLocal(IDC_SERVICE, GET_TEMPLATE);

		// Execute service
		serviceBinder = executeService(serviceBinder, USER);

		// Return PdfwRulesDRS  ResultSet
		return serviceBinder.getResultSet(TEMPLATE_RESULT);
                

	}

	/**
	 * This method executes PDFW_SAVE_NEW_TEMPLATE
	 * @param ResultSet PdfwTextWatermarks from service PDFW_SAVE_NEW_TEMPLATE.
	 * @throws IdcClientException
	 */
	private static void savePdfwTemplates(final DataResultSet PdfwTextWatermarks) throws IdcClientException {
		// DataBinder for service call
		final DataBinder serviceBinder = getNewDataBinder();

		// Set service parameters for LocalData - used the default values for all except PDFW Template ID(content id) and Title 
		        serviceBinder.putLocal(IDC_SERVICE, SAVE_TEMPLATE);
                serviceBinder.putLocal("pdfwTemplateID","RIDC_Template_4");
                serviceBinder.putLocal("pdfwTemplateTitle","RIDC_Template_4");               
                serviceBinder.putLocal("pdfwAllowCopy","YES");
                serviceBinder.putLocal("pdfwAllowPrinting","YES");
                serviceBinder.putLocal("pdfwOwnerPassword","");
                serviceBinder.putLocal("pdfwPasswordsEncrypted", "0");
                serviceBinder.putLocal("pdfwSecurityLevel","NONE");
                serviceBinder.putLocal("pdfwTemplateVersion","1.3");
                serviceBinder.putLocal("pdfwUserPassword",""); 

               /** 
                 * Adding the 13 parameters which are part of PdfwTextWatermarks result set 
               */
                
               PdfwTextWatermarks.addField(TEXT,"");
               PdfwTextWatermarks.addField(LOCATION,"");
               PdfwTextWatermarks.addField(ROTATION,"");
               PdfwTextWatermarks.addField(ALIGNMENT,"");
               PdfwTextWatermarks.addField(FONTNAME,"");
               PdfwTextWatermarks.addField(FONTSIZE,"");
               PdfwTextWatermarks.addField(FONTWEIGHT,"");
               PdfwTextWatermarks.addField(FONTCOLOR,"");
               PdfwTextWatermarks.addField(LAYER,"");
               PdfwTextWatermarks.addField(PAGERANGE,"");
               PdfwTextWatermarks.addField(PAGERANGEMODIFIER,"");
               PdfwTextWatermarks.addField(X_COORD,"");
               PdfwTextWatermarks.addField(Y_COORD,"");

                 /**
                 * Array list initialized to pass the values to 13  parameters in PdfwTextWatermarks resultset 
                 * Have used the default values in this example code 
                 */
          
                ArrayList<String> rowList = new ArrayList<String>(13);
                rowList.add("FROM_RIDC_constants");
                rowList.add("CENTER");
                rowList.add("0");
                rowList.add("ALIGN_CENTER");
                rowList.add("COURIER");
                rowList.add("36");  
                rowList.add("NORMAL");
                rowList.add("BLACK");
                rowList.add("OVER");
                rowList.add("");
                rowList.add("ALL"); 
                rowList.add("0");
                rowList.add("0");
                
                PdfwTextWatermarks.addRow(rowList);
                
                serviceBinder.addResultSet("PdfwTextWatermarks",PdfwTextWatermarks);
                
                // Execute service
		executeService(serviceBinder, USER);
	}



          /**
            * Result set for Image Watermarks
            * 8 parameters in PdfwImageWatermarks Result set
           */

/*          private static void savePdfwImageTemplates(final DataResultSet PdfwImageWatermarks) throws IdcClientException {
                
               final DataBinder serviceBinder = getNewDataBinder();
			   
			   //LocalData section from text watermarks to copied here for use when creating Image watermarks 

               PdfwImageWatermarks.addField(IMAGEID,"");
               PdfwImageWatermarks.addField(LOCATION,"");
               PdfwImageWatermarks.addField(LAYER,"");
               PdfwImageWatermarks.addField(PAGERANGE,"");
               PdfwImageWatermarks.addField(PAGERANGEMODIFIER,"");
               PdfwImageWatermarks.addField(X_COORD,"");
               PdfwImageWatermarks.addField(Y_COORD,"");
               PdfwImageWatermarks.addField(SCALEFACTOR,"");

                ArrayList<String> rowList = new ArrayList<String>(8);
                rowList.add("");
                rowList.add("");
                rowList.add("");
                rowList.add("");
                rowList.add("");
                rowList.add("");
                rowList.add("");
                rowList.add("");
                
                PdfwImageWatermarks.addRow(rowList);
                
                serviceBinder.addResultSet("PdfwImageWatermarks",PdfwImageWatermarks);
                
                // Execute service
                executeService(serviceBinder, USER);
        }
  
   /**
     * Image Water marks code part- End 
    */
 


   /** Resultset for Electronic Signature Watermarks
     * 11 parameters in PdfwSignatureWatermarks resultset
   */   
    
         
             /*
              private static void savePdfwSignatureTemplates(final DataResultSet PdfwSignatureWatermarks) throws IdcClientException {
                
               final DataBinder serviceBinder = getNewDataBinder();
			   
			   // LocalData section from text watermarks to copied here for use when creating Signature watermarks 
               
               PdfwSignatureWatermarks.addField(BLOCKLABEL,"");
               PdfwSignatureWatermarks.addField(FIELDS,"");
               PdfwSignatureWatermarks.addField(LOCATION,"");
               PdfwSignatureWatermarks.addField(FONTNAME,"");
               PdfwSignatureWatermarks.addField(FONTSIZE,"");
               PdfwSignatureWatermarks.addField(FONTCOLOR,"");
               PdfwSignatureWatermarks.addField(LAYER,"");
               PdfwSignatureWatermarks.addField(PAGERANGE,"");
               PdfwSignatureWatermarks.addField(PAGERANGEMODIFIER,"");
               PdfwSignatureWatermarks.addField(X_COORD,"");
               PdfwSignatureWatermarks.addField(Y_COORD,"");

                ArrayList<String> rowList = new ArrayList<String>(11);
                rowList.add("");
                rowList.add("");
                rowList.add("");
                rowList.add("");
                rowList.add("");
                rowList.add("");
                rowList.add("");
                rowList.add("");
                rowList.add("");
                rowList.add("");
                rowList.add("");
                
                PdfwSignatureWatermarks.addRow(rowList);
                
                serviceBinder.addResultSet("PdfwSignatureWatermarks",PdfwSignatureWatermarks);
                
                // Execute service
                executeService(serviceBinder, USER);
        }
      
/** 
  * Signature watermark code part end 
*/

	/**
	 * This method executes a service.
	 * @param binder DataBinder containing service parameters.
	 * @param userName User to execute service as.
	 * @return DataBinder from service call.
	 * @throws IdcClientException
	 * @throws IOException
	 */
	public static DataBinder executeService(DataBinder binder, final String userName) throws IdcClientException {
		final HdaBinderSerializer serializer = new HdaBinderSerializer (UTF8, getIntradocClient().getDataFactory ());

		try {
			System.out.println("Service binder before executing:");
			serializer.serializeBinder (System.out, binder);
		} catch (final IOException ioe) {
			ioe.printStackTrace(System.out);
		}

		// Execute the request
	    final ServiceResponse response = getIntradocClient().sendRequest(new IdcContext(userName), binder);
	    
            // Get the response as a DataBinder
	    binder = response.getResponseAsBinder();
		
                 try {
			System.out.println("Service binder after executing:");
			serializer.serializeBinder (System.out, binder);
		} catch (final IOException ioe) {
			ioe.printStackTrace(System.out);
		}

	    // Return the response as a DataBinder
		return binder;
	}

	public static IdcClientManager getIdcClientManager() {
		if (m_idcClientManager == null) {
			// Needed to create IntradocClient
			m_idcClientManager = new IdcClientManager();
		}
		return m_idcClientManager;
	}

	public static IntradocClient getIntradocClient() throws IdcClientException {
		if (m_idcClient == null) {
			// Client to talk to WebCenter Content
			m_idcClient = (IntradocClient)getIdcClientManager().createClient(IDC_PROTOCOL + RIDC_SERVER + ":" + RIDC_PORT);
		}
		return m_idcClient;
	}
	/**
	 * This method gets a new DataBinder.
	 * @return A new DataBinder.
	 * @throws IdcClientException
	 */
	public static DataBinder getNewDataBinder() throws IdcClientException {
		return getIntradocClient().createBinder();
	}
}