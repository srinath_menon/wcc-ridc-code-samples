import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;
import oracle.stellent.ridc.model.DataBinder;
import oracle.stellent.ridc.model.DataObject;
import oracle.stellent.ridc.model.DataResultSet;

/*
 * @author Srinath Menon
 *
 * This is a class used to test the basic functionality
 * of retrieving the metadata value for Framework Folder on Content Server using RIDC.
 * Using the following Service FLD_EDIT_METADATA_RULES_FORM
 * Here the value for Comments is being displayed - it is part of @Properties LocalData , not in ResultSet 
 * hence need to iterate the LocalData and then retrieve the value 
 */

public class GetFrameworkFolderMeta{

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Create a new IdcClientManager
		IdcClientManager manager = new IdcClientManager ();
		try{
			// Create a new IdcClient Connection using idc protocol (i.e. socket connection to Content Server)
			IdcClient idcClient = manager.createClient ("idc://hostnam:intradocserverport");

                        //for using the web connection - start          
                                // IdcClient idcClient = manager.createClient("http://hostname:port/cs/idcplg");
                                // IdcContext userContext = new IdcContext("weblogic", "<password as set>");          
                        //for using web connection - end
                                  
			// Create new context 
			IdcContext userContext = new IdcContext ("weblogic");
                        
			// Create an HdaBinderSerializer; this is not necessary, but it allows us to serialize the request and response data binders
			HdaBinderSerializer serializer = new HdaBinderSerializer ("UTF-8", idcClient.getDataFactory ());
			
			// service request initialize : 
			DataBinder dataBinder = idcClient.createBinder();
            dataBinder.putLocal("IdcService", "FLD_EDIT_METADATA_RULES_FORM");
            dataBinder.putLocal("fFolderGUID","<GUID for the folder which information is to be retrieved>");
            // Write the data binder for the request to stdout
            serializer.serializeBinder (System.out, dataBinder);
            // Send the request to Content Server
            ServiceResponse response = idcClient.sendRequest(userContext,dataBinder);
            // Get the data binder for the response from Content Server
            DataBinder responseData = response.getResponseAsBinder();
            // Write the response data binder to stdout
            serializer.serializeBinder (System.out, responseData);
           
           
             // Retrieve the LocalData  from the data binder-DataObject being used since this the values are part of LocalData - Needed when using this as input for other functions.
             //DataObject localData = responseData.getLocalData();
           
   
            //Printing the value which is required to be displayed 
            System.out.println ("Comments for Framework Folder is   : " + responseData.getLocal("xComments"));
			

		} catch (IdcClientException ice){
			ice.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}

}
