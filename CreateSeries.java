import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;
import java.util.*;
import java.text.*;

/* - Oracle Inc
 * 
 * This code sample is used to create a new Series in WCC:Record server using IdcService : CREATE_SERIES 
 * It would be used to create the Record assets on WCC:Rec server so when using it on the server 
 * need to check the web port and intradoc server port of Record server .
 * dIsParent metadata would be either 0 (if it is a parent)  or the Series ID under which it is being created
 */

public class CreateSeries {

	
	public static void main(String[] args) {

         

   	        // Create a new IdcClientManager
		IdcClientManager manager = new IdcClientManager ();
		try{
			// Create a new IdcClient Connection using idc protocol (i.e. socket connection to Content Server)
			 IdcClient idcClient = manager.createClient ("idc://hostname:intradocserverport");

                                 //for using the web connection - start          
                                // IdcClient idcClient = manager.createClient("http://hostname:16300/urm/idcplg/");
                                IdcContext userContext = new IdcContext("username", "password");          
                                //for using web connection - end
                                  
                        
			// Create an HdaBinderSerializer; this is not necessary, but it allows us to serialize the request and response data binders
			HdaBinderSerializer serializer = new HdaBinderSerializer ("UTF-8", idcClient.getDataFactory ());
			
			// Create series request:
			DataBinder dataBinder = idcClient.createBinder();
            
            
            dataBinder.putLocal("IdcService","CREATE_SERIES");
            dataBinder.putLocal("dSeriesName","<Name of series>");
            dataBinder.putLocal("dSeriesID","<Series ID>");
            dataBinder.putLocal("dDocAuthor","<author for the series>");
            dataBinder.putLocal("dSecurityGroup","<Security group for series>");    
            dataBinder.putLocal("dSeriesDescription","<Series Description>");
            dataBinder.putLocal("dParentSeriesID","<depending on scenario>");
            dataBinder.putLocal("dIsRootSeries","0");
            dataBinder.putLocal("dIsHidden","0");

            serializer.serializeBinder (System.out, dataBinder);
            // Send the request to Content Server
            ServiceResponse response = idcClient.sendRequest(userContext,dataBinder);
            // Get the data binder for the response from Content Server
            DataBinder responseData = response.getResponseAsBinder();
            // Write the response data binder to stdout
            serializer.serializeBinder (System.out, responseData);

			
		} catch (IdcClientException ice){
			ice.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}

}
