import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;
import java.util.*;
import java.text.*;

/*
 * @author Srinath Menon 
 * 
 * This is a class used to create clone/copy of an existing content item on WCC using RIDC 
 * It is achieved using Idc Service COPY_REVISION and the original content item dID, dDocName parameters.
 * Point to keep note of is when using this service with Folders_g , content in a folder would be failing if the service is executed without xCollectionID parameter.
 * Reason being Folders_g does not allow contents with same original name . To workaround that either xCollectionID should be blank(no folders) or set to a separate folder.
 * With Framework Folders by default the folder is not set with the above service.
 */

public class CloneContent {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

         

   	        // Create a new IdcClientManager
		IdcClientManager manager = new IdcClientManager ();
		try{
			// Create a new IdcClient Connection using idc protocol (i.e. socket connection to Content Server)
			 IdcClient idcClient = manager.createClient ("idc://hostname:<intradoc port>");

                                 //for using the web connection - start          
                                // IdcClient idcClient = manager.createClient("http://hostname:16200/cs/idcplg/");
                                IdcContext userContext = new IdcContext("weblogic", "<password>");          
                                //for using web connection - end
                                  

			// Create an HdaBinderSerializer; this is not necessary, but it allows us to serialize the request and response data binders
			HdaBinderSerializer serializer = new HdaBinderSerializer ("UTF-8", idcClient.getDataFactory ());
			
			// Databinder for Creating the clone request
			DataBinder dataBinder = idcClient.createBinder();
            dataBinder.putLocal("IdcService", "COPY_REVISION");
            //Original Content dID and Content Id (dDocname) //
			dataBinder.putLocal("dID","5001");
            dataBinder.putLocal("dDocName","ECMPS5_005001");
			
			//clone item Content ID - this is not mandatory - If set to Auto Content ID this need not be added //
            dataBinder.putLocal("newdDocName","Cloned_item");
			
			//If the original content is in a Contribution Folder (Folders_g) then below parameter is mandatory - Either blank Collection ID or set a new Folder ID 
			//In case of Framework Folder this parameter is not needed 
            dataBinder.putLocal("xCollectionID","");
			
			//Clone Item's Content Title 
            dataBinder.putLocal("dDocTitle","Title-cloned");
            
			//Any other new / custom metadata for Clone Item 
			dataBinder.putLocal("Custom","Clone Item Metadata");
			
			
            serializer.serializeBinder (System.out, dataBinder);
            // Send the request to Content Server
            ServiceResponse response = idcClient.sendRequest(userContext,dataBinder);
            // Get the data binder for the response from Content Server
            DataBinder responseData = response.getResponseAsBinder();
            // Write the response data binder to stdout
            serializer.serializeBinder (System.out, responseData);


 
			
		} catch (IdcClientException ice){
			ice.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}
  }