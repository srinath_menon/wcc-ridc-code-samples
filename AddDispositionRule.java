import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;
import java.util.*;
import java.text.*;

/*
 * @author Srinath Menon
 * 
 * This is a sample code used to create new Disposition rule for existing Record Category using IdcService : EDIT_CATEGORY_DISPOSITIONS
 * The key here is the parameter :dDispositionList 
 * This parameter takes 25 sub-parameters and each of them & their values should be set in databinder using \n delimiter.
 */

public class AddDispositionRule {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

         

   	        // Create a new IdcClientManager
		IdcClientManager manager = new IdcClientManager ();
		try{
			// Create a new IdcClient Connection using idc protocol (i.e. socket connection to Content Server)
			 IdcClient idcClient = manager.createClient ("idc://<urmhostname>:<intradocserver port>");

                                 //for using the web connection - start          
                                // IdcClient idcClient = manager.createClient("http://<urm hostname>:<webport>/urm/idcplg/");
                                IdcContext userContext = new IdcContext("<user id>", "<password>");          
                                //for using web connection - end
                                  
			// Create new context using the 'sysadmin' user
		        //IdcContext userContext = new IdcContext ("weblogic","welcome1");
                        
			// Create an HdaBinderSerializer; this is not necessary, but it allows us to serialize the request and response data binders
			HdaBinderSerializer serializer = new HdaBinderSerializer ("UTF-8", idcClient.getDataFactory ());
			
			// Databinder Initialized
			DataBinder dataBinder = idcClient.createBinder();            
            
            dataBinder.putLocal("IdcService","EDIT_CATEGORY_DISPOSITIONS");
            dataBinder.putLocal("dCategoryID","RIDCTest2"); //Category ID for which disposition is to be added
            dataBinder.putLocal("dFolderID","RIDCTest2"); //Category name for which disposition is to be added
			
			/*dDispositionList parameter and it's values*/
            dataBinder.putLocal("dDispositionList","25\ndDispEventTrigger\ndDispositionID\ndPreviousID\ndDispOrder\ndDispPeriod\ndDispPeriodUnits\ndDispAction\ndIsExtApprovalProcess\nDispReviewer\nRmaDispositionAdvancedGrouping\ndFolderID\ndDispRulesApplyTo\ndLocationType\ndDispLocation\ndDispLocation2\ndParameters\ndFieldMap\ndDispCutoffCount\ndDispTriggerType\ndIsSystemDerived\ndDerivedTriggerType\ndDerivedEventTrigger\ndDerivedMonthDelay\ndDerivedDayDelay\nRmaDispositionAdvancedGrouping\nwwRmaExpired\n\n\n0\n1\nwwRmaMonth\nwwRmaMarkExpired\n\nweblogic\n\n\n3\n\n\n\n\n\n\nSubjectTriggerList\n0\n\n\n\n\n\n\n");

            serializer.serializeBinder (System.out, dataBinder);
            // Send the request to Content Server
            ServiceResponse response = idcClient.sendRequest(userContext,dataBinder);
            // Get the data binder for the response from Content Server
            DataBinder responseData = response.getResponseAsBinder();
            // Write the response data binder to stdout
            serializer.serializeBinder (System.out, responseData);

			
		} catch (IdcClientException ice){
			ice.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}

}
