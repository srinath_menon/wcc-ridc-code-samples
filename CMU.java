import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;
import java.util.*;
import java.text.*;
import java.util.zip.ZipInputStream;

/* Author - Srinath Menon
*This a RIDC code which will upload a CMU bundle to UCM and then import the same . 

*Parameters to be passed are :

*filename - This is needed for CMU bundle upload action , where in the zip file name has to be passed.
*taskname - The name of the uploaded bundle  which should be imported . It is usually the filename sans .zip extension

*/

public class CMU {

	/**
	 * @param args
	 */


               InputStream fileStream = null;           
               private static final String filename = ""; 
               private static final String taskname = "";
   	        
               // RIDC connection information will be read from config.file which is external to this code
               private static final String IDC_PROTOCOL = "";
               private static final String RIDC_SERVER = "";
               private static final String RIDC_PORT = "";
               private static IdcClientManager m_idcClientManager;
               private static IntradocClient m_idcClient;
               private static final String UTF8 = "UTF-8";
               private static final String USER = "";
               
			   // Variables initialized for properties file
               private static Properties prop = new Properties();
	           private static InputStream input = null;
           

       public static DataBinder executeService(DataBinder binder, final String userName) throws IdcClientException {
                final HdaBinderSerializer serializer = new HdaBinderSerializer (UTF8, getIntradocClient().getDataFactory ());

                try {
                        System.out.println("Service binder before executing:");
                        serializer.serializeBinder (System.out, binder);
                } catch (final IOException ioe) {
                        ioe.printStackTrace(System.out);
                }

                // Execute the request
                final ServiceResponse response = getIntradocClient().sendRequest(new IdcContext(userName), binder);

                // Get the response as a DataBinder
                binder = response.getResponseAsBinder();

                 try {
                        System.out.println("Service binder after executing:");
                        serializer.serializeBinder (System.out, binder);
                } catch (final IOException ioe) {
                        ioe.printStackTrace(System.out);
                }

                // Return the response as a DataBinder
                return binder;
        }

        public static IdcClientManager getIdcClientManager() {
                if (m_idcClientManager == null) {
                        // Needed to create IntradocClient
                        m_idcClientManager = new IdcClientManager();
                }
                return m_idcClientManager;
        }

        public static IntradocClient getIntradocClient() throws IdcClientException {
               
                     try{ 
			            //Properties file initialized  		 
                        input = new FileInputStream("config.file");  
		                prop.load(input);
                     
                if (m_idcClient == null) {
                        //Client to talk to WebCenter Content
                        //m_idcClient = (IntradocClient)getIdcClientManager().createClient(IDC_PROTOCOL + RIDC_SERVER + ":" + RIDC_PORT);
                          m_idcClient = (IntradocClient)getIdcClientManager().createClient(prop.getProperty("IDC_PROTOCOL")+"://" + prop.getProperty("RIDC_SERVER") + ":" + prop.getProperty("RIDC_PORT"));
                }
               } catch (IOException ioe){
                        ioe.printStackTrace();
                      }
              return m_idcClient;
        }
        /**
         * This method gets a new DataBinder.
         * @return A new DataBinder.
         * @throws IdcClientException
         */
        public static DataBinder getNewDataBinder() throws IdcClientException {
                return getIntradocClient().createBinder();
        }


       /**
	      *Main class
	   */
       public static void main(final String[] args){
                        
                        UploadCMU(filename);
                        ImportCMU(taskname);

                } 


                                 	
      /**
	      *Databinder for uploading zip bundle
	   */
      private static void UploadCMU(String filename) {
                      
                      try{ 
                       System.out.println("Inside Upload method"); 

                        final DataBinder serviceBinder = getNewDataBinder();
                        serviceBinder.putLocal("IdcService", "CMU_UPLOAD_BUNDLE");
                        serviceBinder.addFile("bundleName",new File(prop.getProperty("filename")));
                        serviceBinder.putLocal("createExportTemplate","1");
                        serviceBinder.putLocal("forceBundleOverwrite","1");
                        // Execute service
                        executeService(serviceBinder, prop.getProperty("user"));
                      } catch (IdcClientException ice){
                        ice.printStackTrace();
                      }
                         catch (IOException ioe){
                        ioe.printStackTrace();
                      }

                  }                         

    /**
	      *Databinder for importing zip bundle
	   */
     private static void ImportCMU(String taskname){
                        try {
                        System.out.println("Inside Import method");
                        final DataBinder serviceBinder = getNewDataBinder();
                        serviceBinder.putLocal("IdcService", "CMU_CREATE_ACTION");
                        serviceBinder.putLocal("TaskName",prop.getProperty("taskname"));
                        serviceBinder.putLocal("isImport","1");
                        // Execute service
                        executeService(serviceBinder, prop.getProperty("user"));
  
                      } catch (IdcClientException ice){
                        ice.printStackTrace();
                      }
                  }
                     
}
