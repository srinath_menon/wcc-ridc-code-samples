import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;

/*
 * @author Srinath Menon
 * 
 * This is a class used to retrieve all the Username and Auth type for users who have logged in to WCC Server atleast once
 * This is default behavior since std_resources.htm file has the following line added - OOTB - datasource for Users already created
            <tr>
              <td>Users</td>
                <td>SELECT Users.* FROM Users
              </td>
                  <td>Yes</td>
             </tr>

 * This line is added for GET_DATARESULTSET to be able to look for data source - Users and execute the query
 */

public class GetUsers {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Create a new IdcClientManager
		IdcClientManager manager = new IdcClientManager ();
		try{
			// Create a new IdcClient Connection using idc protocol (i.e. socket connection to Content Server)
			IdcClient idcClient = manager.createClient ("idc://hostname:<intradocserverport>");

                                 //for using the web connection - start          
                                // IdcClient idcClient = manager.createClient("http://hostname:webport/cs/idcplg");
                                 IdcContext userContext = new IdcContext("weblogic", "<password>");          
                                //for using web connection - end
                                  
                        
			// Create an HdaBinderSerializer; this is not necessary, but it allows us to serialize the request and response data binders
			HdaBinderSerializer serializer = new HdaBinderSerializer ("UTF-8", idcClient.getDataFactory ());
			
			// Databinder for service request
			DataBinder dataBinder = idcClient.createBinder();
            dataBinder.putLocal("IdcService", "GET_DATARESULTSET");
            dataBinder.putLocal("dataSource","Users");//Users datasource is already present in std_resource.htm file
            dataBinder.putLocal("resultName","UserList");//Resultname can be named arbitrarily based on choice. Same should be used later on for iterating over to list the details.
            // Write the data binder for the request to stdout
            serializer.serializeBinder (System.out, dataBinder);
            // Send the request to Content Server
            ServiceResponse response = idcClient.sendRequest(userContext,dataBinder);
            // Get the data binder for the response from Content Server
            DataBinder responseData = response.getResponseAsBinder();
            // Write the response data binder to stdout
            serializer.serializeBinder (System.out, responseData);
           
            // Retrieve the SecurityGroups ResultSet from the response
            DataResultSet resultSet1 = responseData.getResultSet("UserList");

            // Iterate over the ResultSet and list all the Security Groups present on WCC server - Authtype will list if the user is external or internal
            for (DataObject dataObject : resultSet1.getRows ()) {

               System.out.println ("User Name is  : " + dataObject.get ("dName") );
               System.out.println ("User Authentication Type is : " + dataObject.get ("dUserAuthType"));

             }

			
		} catch (IdcClientException ice){
			ice.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}

}
