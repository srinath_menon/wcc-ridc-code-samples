import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;
import java.util.*;
import java.text.*;

/*
 * @author Srinath Menon
 * 
 * This is a class used to test the basic functionality of retrieving the set of contents which are added as Related Content to a existing
 * content item using GET_RELATED_CONTENT service .
   
 *  dLinkTypeID is set to 1 . Reason being that the related content of type "rendition" is being looked up here . 
 
 *  This value would be as follows :
 *  dLinkTypeID        Related content type 
 *   1                    Rendition
 *   2                    Supersedes
 *   3                    Has Supporting content
 *   4                    Cross References

* 2 more sub types are : 

*    Supports - dLinkTypeID=3 and extraparameter is isGetParents=1
*    Cross Referenced By - dLinkTypeID=4 and extraparameter is isGetParents=1 
* For eg : 
*      dataBinder.putLocal("dLinkTypeID","3"); 
*      dataBinder.putLocal("isGetParents","1"); 
 */

public class GetRelatedContent {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

         

   	        // Create a new IdcClientManager
		IdcClientManager manager = new IdcClientManager ();
		try{
			// Create a new IdcClient Connection using idc protocol (i.e. socket connection to Content Server)
			    IdcClient idcClient = manager.createClient("idc://hostname:<intradocserver port>");

                                 //for using the web connection - start          
                                // IdcClient idcClient = manager.createClient("http://hostnam:port/cs/idcplg/");
                                IdcContext userContext = new IdcContext("weblogic", "<pwd>");          
                                //for using web connection - end
                                  
                        
			// Create an HdaBinderSerializer; this is not necessary, but it allows us to serialize the request and response data binders
			HdaBinderSerializer serializer = new HdaBinderSerializer ("UTF-8", idcClient.getDataFactory ());
			
			// Databinder for service request
			DataBinder dataBinder = idcClient.createBinder();
            dataBinder.putLocal("IdcService", "GET_RELATED_CONTENT");
            dataBinder.putLocal("dSource","CS");
            dataBinder.putLocal("dID","<dID of the parent for which list is to be created>");
            dataBinder.putLocal("dLinkTypeID","1");  //based on what relation content is to be retrieved
 
            serializer.serializeBinder (System.out, dataBinder);
            // Send the request to Content Server
            ServiceResponse response = idcClient.sendRequest(userContext,dataBinder);
            // Get the data binder for the response from Content Server
            DataBinder responseData = response.getResponseAsBinder();
            // Write the response data binder to stdout
            serializer.serializeBinder (System.out, responseData);
            // Retrieve the SearchResults ResultSet from the response - Name of result set - RelatedContent
            DataResultSet resultSet = responseData.getResultSet("RelatedContent");

            // Iterate over the ResultSet, retrieve data - Content ID 
            for (DataObject dataObject : resultSet.getRows ()) {

               System.out.println ("Related ContentID is  : " + dataObject.get ("dDocName") );
               }

 
			
		} catch (IdcClientException ice){
			ice.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}

}
