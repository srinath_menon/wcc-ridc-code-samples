import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;
import java.util.*;
import java.text.*;

/*
 * @author Srinath Menon - Oracle Inc
 * 
 * This is a class used to retrieve PDF Watermark Rules and Templates through RIDC .
 *  Service used PDFW_GET_RULES
 */

public class GetPDFWAssets {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

         

   	        // Create a new IdcClientManager
		IdcClientManager manager = new IdcClientManager ();
		try{
			// Create a new IdcClient Connection using idc protocol (i.e. socket connection to Content Server)
			 
                         IdcClient idcClient = manager.createClient("idc://<ucm hostname>:<intradocserverport>");

                                 //for using the web connection - start          
                                // IdcClient idcClient = manager.createClient("http://<ucmhostname>:<web port>/cs/idcplg/");
                                IdcContext userContext = new IdcContext("weblogic", "<password>");          
                                //for using web connection - end
                                  
			                        
			// Create an HdaBinderSerializer; this is not necessary, but it allows us to serialize the request and response data binders
			HdaBinderSerializer serializer = new HdaBinderSerializer ("UTF-8", idcClient.getDataFactory ());
			
			// Databinder for PDFW retrieve request
			DataBinder dataBinder = idcClient.createBinder();
            dataBinder.putLocal("IdcService", "PDFW_GET_RULES");
 
            serializer.serializeBinder (System.out, dataBinder);
            // Send the request to Content Server
            ServiceResponse response = idcClient.sendRequest(userContext,dataBinder);
            // Get the data binder for the response from Content Server
            DataBinder responseData = response.getResponseAsBinder();
            // Write the response data binder to stdout
            serializer.serializeBinder (System.out, responseData);
            // Retrieve the  ResultSet from the response - ruleSet for Rules 
            DataResultSet ruleSet = responseData.getResultSet("PdfwRulesDRS");
			
			// Retrieve the  ResultSet from the response - templateSet for Templates 
            DataResultSet templateSet = responseData.getResultSet("PdfwTemplates");

            // Iterate over the ResultSet, retrieve data - for each of the assets - Rules and Templates respectively
            for( DataObject dataObject : ruleSet.getRows ()) {

               System.out.println ("PDFW Rule Name is   : " + dataObject.get ("pdfwRuleName") );
               System.out.println ("Template ID attached to PDFW Rule is  :    : " + dataObject.get ("pdfwTemplateID") );
               }

            for ( DataObject dataObject : templateSet.getRows () ) {

               System.out.println ("Title for PDFW Template is :    : " + dataObject.get ("dDocTitle") );
			   System.out.println ("Criteria for Rule is  : " + dataObject.get ("pdfwCriteria") );
			   System.out.println ("ContentID of PDF Template is : " + dataObject.get ("dDocName") );
               }

   	   		
		} catch (IdcClientException ice){
			ice.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}

}
