import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;
import java.util.*;
import java.text.*;

/*
 * @author Srinath Menon
 * 
 * This is a class used to list all existing Credential Maps and their values using RIDC
 */

public class GetCredMap {

	/**
	 * @param args
	 */
	public static void main(String[] args) {


         

   	        // Create a new IdcClientManager
		IdcClientManager manager = new IdcClientManager ();
		try{
			// Create a new IdcClient Connection using idc protocol (i.e. socket connection to Content Server)
			 IdcClient idcClient = manager.createClient ("idc://<ucm hostname>:<intradocserverport>");

                                //for using the web connection - start          
                                       // IdcClient idcClient = manager.createClient("http://<hostname>:<webport>/cs/idcplg/");
                                //for using web connection - end
                                  
			// Create new context using the 'weblogic' user
		        IdcContext userContext = new IdcContext ("weblogic","<pwd>");
                        
			// Create an HdaBinderSerializer; this is not necessary, but it allows us to serialize the request and response data binders
			HdaBinderSerializer serializer = new HdaBinderSerializer ("UTF-8", idcClient.getDataFactory ());
			
			// Databinder for getting Credential Map request
			DataBinder dataBinder = idcClient.createBinder();
            dataBinder.putLocal("IdcService", "GET_CREDENTIALS_MAP");

            
            // Write the data binder for the request to stdout
            serializer.serializeBinder (System.out, dataBinder);
            // Send the request to Content Server
            ServiceResponse response = idcClient.sendRequest(userContext,dataBinder);
            // Get the data binder for the response from Content Server
            DataBinder responseData = response.getResponseAsBinder();
            // Write the response data binder to stdout
            serializer.serializeBinder (System.out, responseData);
            
            // Retrieve the CredentialMaps ResultSet from the response
            DataResultSet resultSet = responseData.getResultSet("CredentialMaps");

            // Iterate over the ResultSet, retrieve credential map id and map values.
            for (DataObject dataObject : resultSet.getRows ()) {

               System.out.println (" Credentail Map ID is  : " + dataObject.get ("cmMapId") );
               System.out.println (" Map is as follows : " + dataObject.get ("cmMapValue"));

             }

 
			
		} catch (IdcClientException ice){
			ice.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}

}
