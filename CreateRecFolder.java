import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;
import java.util.*;
import java.text.*;
/*
 * @author Srinath Menon
 * 
 * This is a sample code used to create Record Folder using IdcService : CREATE_FOLDER
 */

public class import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;
import java.util.*;
import java.text.*;
/*
 * @author Srinath Menon
 * 
 * This is a sample code used to create Record Folder using IdcService : CREATE_FOLDER
 */

public class CreateRecFolder {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

         

   	        // Create a new IdcClientManager
		IdcClientManager manager = new IdcClientManager ();
		try{
			// Create a new IdcClient Connection using idc protocol (i.e. socket connection to Content Server)
			 IdcClient idcClient = manager.createClient ("idc://<hostname>:<intradoc server port>");

                                 //for using the web connection - start          
                                // IdcClient idcClient = manager.createClient("http://hostname:port/cs/idcplg/");
                                IdcContext userContext = new IdcContext("weblogic", "welcome1");          
                                //for using web connection - end
              
			// Create an HdaBinderSerializer; this is not necessary, but it allows us to serialize the request and response data binders
			HdaBinderSerializer serializer = new HdaBinderSerializer ("UTF-8", idcClient.getDataFactory ());
			
			// Databinder for Creating Record Folder request
			DataBinder dataBinder = idcClient.createBinder();
            
            
            dataBinder.putLocal("IdcService","CREATE_FOLDER");
            dataBinder.putLocal("dFolderName","<name of the folder>");
            dataBinder.putLocal("dFolderID","<Folder ID>");
           // dataBinder.putLocal("dFolderDescription","<description for the folder>");
            dataBinder.putLocal("dDocAuthor","<username>");
            dataBinder.putLocal("dSecurityGroup","<group assigned>");    
            dataBinder.putLocal("dCategoryID","<under which RC this folder is to be created>");
            dataBinder.putLocal("dIsVital","0");  // Yes =1 , No=0
           // dataBinder.putLocal("dRecordActivationDate","");
           // dataBinder.putLocal("dRecordExpirationDate","");
           // dataBinder.putLocal("dDeleteApproveDate","");
           // dataBinder.putLocal("dSupplementalMarkings","");
            dataBinder.putLocal("dRMProfileTrigger","");
           // dataBinder.putLocal("dVitalReviewer","");
           // dataBinder.putLocal("dVitalPeriodUnits","0");
           // dataBinder.putLocal("dVitalPeriod","0");
            serializer.serializeBinder (System.out, dataBinder);
            // Send the request to Content Server
            ServiceResponse response = idcClient.sendRequest(userContext,dataBinder);
            // Get the data binder for the response from Content Server
            DataBinder responseData = response.getResponseAsBinder();
            // Write the response data binder to stdout
            serializer.serializeBinder (System.out, responseData);

			
		} catch (IdcClientException ice){
			ice.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}

}
 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

         

   	        // Create a new IdcClientManager
		IdcClientManager manager = new IdcClientManager ();
		try{
			// Create a new IdcClient Connection using idc protocol (i.e. socket connection to Content Server)
			 IdcClient idcClient = manager.createClient ("idc://<hostname>:<intradoc server port>");

                                 //for using the web connection - start          
                                // IdcClient idcClient = manager.createClient("http://hostname:port/cs/idcplg/");
                                IdcContext userContext = new IdcContext("weblogic", "welcome1");          
                                //for using web connection - end
              
			// Create an HdaBinderSerializer; this is not necessary, but it allows us to serialize the request and response data binders
			HdaBinderSerializer serializer = new HdaBinderSerializer ("UTF-8", idcClient.getDataFactory ());
			
			// Databinder for Creating Record Folder request
			DataBinder dataBinder = idcClient.createBinder();
            
            
            dataBinder.putLocal("IdcService","CREATE_FOLDER");
            dataBinder.putLocal("dFolderName","<name of the folder>");
            dataBinder.putLocal("dFolderID","<Folder ID>");
           // dataBinder.putLocal("dFolderDescription","<description for the folder>");
            dataBinder.putLocal("dDocAuthor","<username>");
            dataBinder.putLocal("dSecurityGroup","<group assigned>");    
            dataBinder.putLocal("dCategoryID","<under which RC this folder is to be created>");
            dataBinder.putLocal("dIsVital","0");  // Yes =1 , No=0
           // dataBinder.putLocal("dRecordActivationDate","");
           // dataBinder.putLocal("dRecordExpirationDate","");
           // dataBinder.putLocal("dDeleteApproveDate","");
           // dataBinder.putLocal("dSupplementalMarkings","");
            dataBinder.putLocal("dRMProfileTrigger","");
           // dataBinder.putLocal("dVitalReviewer","");
           // dataBinder.putLocal("dVitalPeriodUnits","0");
           // dataBinder.putLocal("dVitalPeriod","0");
            serializer.serializeBinder (System.out, dataBinder);
            // Send the request to Content Server
            ServiceResponse response = idcClient.sendRequest(userContext,dataBinder);
            // Get the data binder for the response from Content Server
            DataBinder responseData = response.getResponseAsBinder();
            // Write the response data binder to stdout
            serializer.serializeBinder (System.out, responseData);

			
		} catch (IdcClientException ice){
			ice.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}

}
