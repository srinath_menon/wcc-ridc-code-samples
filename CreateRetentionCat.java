import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;
import java.util.*;
import java.text.*;

/*
 * @author Srinath Menon
 * 
 * This is a sample code used to create Retention Category using IdcService : CREATE_CATEGORY
 * It would be used to create the Record assets on WCC:Rec server so when using it on the server 
 * need to check the web port and intradoc server port of Record server .
 */

public class CreateRetentionCat {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

         

   	    // Create a new IdcClientManager
		IdcClientManager manager = new IdcClientManager ();
		try{
			// Create a new IdcClient Connection using idc protocol (i.e. socket connection to Content Server)
			 IdcClient idcClient = manager.createClient ("idc://hostname:intradocport");

                                 //for using the web connection - start          
                                // IdcClient idcClient = manager.createClient("http://hostname:16300/urm/idcplg/");
                                IdcContext userContext = new IdcContext("username", "password");          
                                //for using web connection - end
                                  
			                            
			// Create an HdaBinderSerializer; this is not necessary, but it allows us to serialize the request and response data binders
			HdaBinderSerializer serializer = new HdaBinderSerializer ("UTF-8", idcClient.getDataFactory ());
			
			// Databinder for create category : 
			DataBinder dataBinder = idcClient.createBinder();
            
            
            dataBinder.putLocal("IdcService","CREATE_CATEGORY");
            dataBinder.putLocal("dCategoryName","RIDC-RetCat-2");
            dataBinder.putLocal("dCategoryID","RIDC-RetCat-2");
            dataBinder.putLocal("dDocAuthor","weblogic");
            dataBinder.putLocal("dSecurityGroup","RecordsGroup");    
            dataBinder.putLocal("dCategoryDescription","RIDC-RetCat-2");
            dataBinder.putLocal("dDispAuthority","weblogic");
            dataBinder.putLocal("dIsNoRevisionsCategory","0");
            dataBinder.putLocal("dIsNoDeleteCategory","0");
            dataBinder.putLocal("dIsNoEditCategory","0");
            dataBinder.putLocal("dIsPermanent","0");
            dataBinder.putLocal("dDispositionType","0");
            dataBinder.putLocal("dIsVital","0");
            dataBinder.putLocal("dRMProfileTrigger","");
            dataBinder.putLocal("dSeriesID" ,"0");
           // dataBinder.putLocal("dVitalReviewer","");
           // dataBinder.putLocal("dVitalPeriodUnits","0");
           // dataBinder.putLocal("dVitalPeriod","0");
           // dataBinder.putLocal("dSecurityScripts","");
           // dataBinder.putLocal("dNotificationScripts",""); 
            dataBinder.putLocal("dDocAccount","");

            serializer.serializeBinder (System.out, dataBinder);
            // Send the request to Content Server
            ServiceResponse response = idcClient.sendRequest(userContext,dataBinder);
            // Get the data binder for the response from Content Server
            DataBinder responseData = response.getResponseAsBinder();
            // Write the response data binder to stdout
            serializer.serializeBinder (System.out, responseData);

			
		} catch (IdcClientException ice){
			ice.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}

}
