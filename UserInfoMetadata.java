import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;

/*
 * @author Srinath Menon
 *
 * This is a class used to retrieve all the values which are stored in User Admin Information Field which are Option Lists type
 * These option lists are not associated with a view , unlike the normal Option list type metadata which creates
 * a view for every metadata which is set as Option list.
 */
public class UserInfoMetadata {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Create a new IdcClientManager
		IdcClientManager manager = new IdcClientManager ();
		try{
			// Create a new IdcClient Connection using idc protocol (i.e. socket connection to Content Server)
		IdcClient idcClient = manager.createClient ("idc://hostname:intradocserverport");

                                 //for using the web connection - start          
                                // IdcClient idcClient = manager.createClient("http://hostname:webport/cs/idcplg");
                                 IdcContext userContext = new IdcContext("weblogic", "<password>");          
                                //for using web connection - end
                                  
                        
			// Create an HdaBinderSerializer; this is not necessary, but it allows us to serialize the request and response data binders
			HdaBinderSerializer serializer = new HdaBinderSerializer ("UTF-8", idcClient.getDataFactory ());
			
			// Databinder for issuing request
			DataBinder dataBinder = idcClient.createBinder();
            dataBinder.putLocal("IdcService", "GET_USER_INFO");
            // Write the data binder for the request to stdout
            serializer.serializeBinder (System.out, dataBinder);
            // Send the request to Content Server
            ServiceResponse response = idcClient.sendRequest(userContext,dataBinder);
            // Get the data binder for the response from Content Server
            DataBinder responseData = response.getResponseAsBinder();
            // Write the response data binder to stdout
            serializer.serializeBinder (System.out, responseData);
            
            //The metadata added for User Admin - Information Field shows up as OptionList in the response hence need to use getOptionList
            // When a field is created and set as "Option List" then the list name is set by default as "Users_<Information Field Name>List"
			List <String> locales  =responseData.getOptionList("Users_TestingList"); 
            
            //This would print all the values in Information field option list 
            System.out.println("Option List Values : " + locales);

		} catch (IdcClientException ice){
			ice.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}

}
