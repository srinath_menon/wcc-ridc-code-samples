import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;
import java.util.*;
import java.text.*;

/*
 * @author Srinath Menon
 * 
 * This is a sample code used to create Framework Folder using IdcService : FLD_CREATE_FOLDER
 */

public class CreateFrameworkFolder {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

         

   	        // Create a new IdcClientManager
		IdcClientManager manager = new IdcClientManager ();
		try{
			// Create a new IdcClient Connection using idc protocol (i.e. socket connection to Content Server)
			 IdcClient idcClient = manager.createClient ("idc://hostname:intradocserverport");

                                 //for using the web connection - start          
                                // IdcClient idcClient = manager.createClient("http://hostname:webport/cs/idcplg/");
                                IdcContext userContext = new IdcContext("weblogic", "<password>");          
                                //for using web connection - end
                                  
			            
			// Create an HdaBinderSerializer; this is not necessary, but it allows us to serialize the request and response data binders
			HdaBinderSerializer serializer = new HdaBinderSerializer ("UTF-8", idcClient.getDataFactory ());
			
			// Databinder for folder creation request
			DataBinder dataBinder = idcClient.createBinder();
            
            
            dataBinder.putLocal("IdcService","FLD_CREATE_FOLDER");
            //GUID of the folder under which this new folder is being created . This value can be retrieved from the DB or UI by using IsJava=1 paremeter 
			dataBinder.putLocal("fParentGUID","FLD_ROOT"); 
			//Name of the new folder being created 
            dataBinder.putLocal("fFolderName","<name>");
            dataBinder.putLocal("fSecurityGroup","Public");     // Security group to which the folder is assigned .
            dataBinder.putLocal("fInhibitPropagation","1"); //Inhibit Propagation value . Either 1 or 0 
            dataBinder.putLocal("fPromptForMetadata","1"); // Prompt for Metadata - used when DIS comes into picture - Values - 0 or 1 
            dataBinder.putLocal("fFolderType","owner"); // optional parameter
            serializer.serializeBinder (System.out, dataBinder);
            // Send the request to Content Server
            ServiceResponse response = idcClient.sendRequest(userContext,dataBinder);
            // Get the data binder for the response from Content Server
            DataBinder responseData = response.getResponseAsBinder();
            // Write the response data binder to stdout
            serializer.serializeBinder (System.out, responseData);

			
		} catch (IdcClientException ice){
			ice.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}

}
