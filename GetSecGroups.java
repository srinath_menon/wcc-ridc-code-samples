import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;

/*
 * @author Srinath Menon 
 * 
 * This is a class used to retrieve all the security groups present in WCC Server 
 * It will only work if std_resources.htm file is edited and following line added : 
 *      <tr>
           <td>SecurityGroups</td>
           <td>select dgroupname from securitygroups order by dgroupname</td>
           <td>Yes</td>
        </tr>

 * This line is added for GET_DATARESULTSET to be able to look for data source - SecurityGroups and execute the query
 * resultName - This is the parameter which sets ResultSet to hold the values - It is arbitrary . For eg here it is set as SGList
 */

public class GetSecGroups {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Create a new IdcClientManager
		IdcClientManager manager = new IdcClientManager ();
		try{
			// Create a new IdcClient Connection using idc protocol (i.e. socket connection to Content Server)
			IdcClient idcClient = manager.createClient ("idc://hostname:<intradocport>");

                                 //for using the web connection - start          
                                // IdcClient idcClient = manager.createClient("http://hostname:webport/cs/idcplg");
                                 IdcContext userContext = new IdcContext("weblogic", "<pwd>");          
                                //for using web connection - end
                                  
                        
			// Create an HdaBinderSerializer; this is not necessary, but it allows us to serialize the request and response data binders
			HdaBinderSerializer serializer = new HdaBinderSerializer ("UTF-8", idcClient.getDataFactory ());
			
			// Databinder for initial service request
			DataBinder dataBinder = idcClient.createBinder();
            dataBinder.putLocal("IdcService", "GET_DATARESULTSET");
            dataBinder.putLocal("dataSource","SecurityGroups"); //SecurityGroups is the name of dataSource added in std_resources.htm file.
            dataBinder.putLocal("resultName","SGList"); //This could be anything per requirement
            // Write the data binder for the request to stdout
            serializer.serializeBinder (System.out, dataBinder);
            // Send the request to Content Server
            ServiceResponse response = idcClient.sendRequest(userContext,dataBinder);
            // Get the data binder for the response from Content Server
            DataBinder responseData = response.getResponseAsBinder();
            // Write the response data binder to stdout
            serializer.serializeBinder (System.out, responseData);
           
            // Retrieve the ResultSet from the response - parameter passed here should match the value set in resultName
            DataResultSet resultSet1 = responseData.getResultSet("SGList");

            // Iterate over the ResultSet and list all the Security Groups present on WCC server
            for (DataObject dataObject : resultSet1.getRows ()) {

               System.out.println ("Security Group Name is  : " + dataObject.get ("dGroupName") );

             }

			
		} catch (IdcClientException ice){
			ice.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}

}
