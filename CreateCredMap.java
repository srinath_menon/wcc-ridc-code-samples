import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;
import java.util.*;
import java.text.*;

/*
 * @author Srinath Menon
 * 
 * This is a class used to create / edit the entries for Credential Map using RIDC.    
 */

public class CreateCredMap {

	/**
	 * @param args
	 */
	public static void main(String[] args) {


         

   	        // Create a new IdcClientManager
		IdcClientManager manager = new IdcClientManager ();
		try{
			// Create a new IdcClient Connection using idc protocol (i.e. socket connection to Content Server)
			 IdcClient idcClient = manager.createClient ("idc://<ucm hostname>:<intradocserver port>");

                                //for using the web connection - start          
                                    // IdcClient idcClient = manager.createClient("http://<ucmhostname>:<webport>/cs/idcplg/");
                                //for using web connection - end
                                  
			// Create new context using the 'weblogic' user
		        IdcContext userContext = new IdcContext ("weblogic","<pwd>");
                        
			// Create an HdaBinderSerializer; this is not necessary, but it allows us to serialize the request and response data binders
			HdaBinderSerializer serializer = new HdaBinderSerializer ("UTF-8", idcClient.getDataFactory ());
			
			// Databinder for creating / editing credential map - ADD_EDIT_CREDENTIALS_MAP service 
			DataBinder dataBinder = idcClient.createBinder();
            dataBinder.putLocal("IdcService", "ADD_EDIT_CREDENTIALS_MAP");
			/**
			* If the mapname is not existing on the server then the service will create a new Credential Map .
			* If user needs to edit the credential map then all the existing mapping should be provided along with the new ones . Or else the existing mapping will be overwritten with the
            * values created in the code.
			* \n is used as new line separator for each of the maps 
			* \\ is used to set \@ special character .
			*/
            dataBinder.putLocal ("mapId","RIDCTest2");
            dataBinder.putLocal("mapNames" ,"RIDCTest2");
            dataBinder.putLocal("mapValue","laxgrp,admin\n laxgrp,sysmanager\n |#all|,%%\n \\@|#all|,\\@%%\n test,admin\n new,test\n deployer,contributor\n");  

            
            // Write the data binder for the request to stdout
            serializer.serializeBinder (System.out, dataBinder);
            // Send the request to Content Server
            ServiceResponse response = idcClient.sendRequest(userContext,dataBinder);
            // Get the data binder for the response from Content Server
            DataBinder responseData = response.getResponseAsBinder();
            // Write the response data binder to stdout
            serializer.serializeBinder (System.out, responseData);
            

 
			
		} catch (IdcClientException ice){
			ice.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}

}
