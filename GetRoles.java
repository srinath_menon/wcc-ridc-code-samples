import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;

/*
 * @author Srinath Menon 
 *
 * This is a class used to retrieve all the Roles  present in WCC Server *
 * It will only work if std_resources.htm file is edited and following line added :
 *      <tr>
           <td>Roles</td>
           <td>select distinct(drolename) from roledefinition order by Drolename</td>
           <td>Yes</td>
        </tr>

 * This line is added for GET_DATARESULTSET to be able to look for data source - Roles and execute the query
 * resultName - this is the result set which will store the values. It can be name anything . For eg here it is RoleList
 */
public class GetRoles {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Create a new IdcClientManager
		IdcClientManager manager = new IdcClientManager ();
		try{
			// Create a new IdcClient Connection using idc protocol (i.e. socket connection to Content Server)
			IdcClient idcClient = manager.createClient ("idc://hostname:<intradocport>");

                                 //for using the web connection - start          
                                // IdcClient idcClient = manager.createClient("http://hostname:webport/cs/idcplg");
                                 IdcContext userContext = new IdcContext("weblogic", "<password>");          
                                //for using web connection - end
                                  
                        
			// Create an HdaBinderSerializer; this is not necessary, but it allows us to serialize the request and response data binders
			HdaBinderSerializer serializer = new HdaBinderSerializer ("UTF-8", idcClient.getDataFactory ());
			
			// Databinder for service request
			DataBinder dataBinder = idcClient.createBinder();
            dataBinder.putLocal("IdcService", "GET_DATARESULTSET");
            dataBinder.putLocal("dataSource","Roles");//Roles is the datasource name set in std_resources.htm file. If anything else is name set there then make change here accordingly.
            dataBinder.putLocal("resultName","RoleList"); //ResultSet name arbitrarily set to hold the values
            // Write the data binder for the request to stdout
            serializer.serializeBinder (System.out, dataBinder);
            // Send the request to Content Server
            ServiceResponse response = idcClient.sendRequest(userContext,dataBinder);
            // Get the data binder for the response from Content Server
            DataBinder responseData = response.getResponseAsBinder();
            // Write the response data binder to stdout
            serializer.serializeBinder (System.out, responseData);
           
            // Retrieve the ResultSet from the response - this should match the value provided for resultName
            DataResultSet resultSet1 = responseData.getResultSet("RoleList");

            // Iterate over the ResultSet, retrieve all the Roles present in WCC server
            for (DataObject dataObject : resultSet1.getRows ()) {

               System.out.println ("Role Name is  : " + dataObject.get ("dRoleName") );

             }

			
		} catch (IdcClientException ice){
			ice.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}

}
