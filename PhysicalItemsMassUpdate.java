import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;
import java.util.*;
import java.text.*;

/*
 * @Author :  Srinath Menon 
 * This RIDC code demonstrates how to mass-update Physical Contents metadata based on search criteria
 * It uses the Records OOTB service to achieve it in 2-steps. 
 */

public class PhysicalItemsMassUpdate {

	/**
	 * @param args
	 */
	public static void main(String[] args) {


        // Create a new IdcClientManager
        		IdcClientManager manager = new IdcClientManager ();
                Properties prop = new Properties();
                InputStream input = null;

		try{
                // Load config file for reading connnection information and input details
                        input = new FileInputStream("config-PhyUpdate.file");
                        prop.load(input);

		     	// Create a new IdcClient Connection using idc protocol (i.e. socket connection to Content Server)
			            IdcClient idcClient = manager.createClient (prop.getProperty("IDC_PROTOCOL")+"://" + prop.getProperty("RIDC_SERVER") + ":" + prop.getProperty("RIDC_PORT"));

                                 //for using the web connection - start          
                                    IdcContext userContext = new IdcContext(prop.getProperty("user"));         
                                 //End
                        
			// Create an HdaBinderSerializer; this is not necessary, but it allows us to serialize the request and response data binders
			            HdaBinderSerializer serializer = new HdaBinderSerializer ("UTF-8", idcClient.getDataFactory ());
			
			// Databinder for search
			            DataBinder dataBinder = idcClient.createBinder();
                        dataBinder.putLocal("IdcService", prop.getProperty("SearchIdcService"));
                        dataBinder.putLocal("ErmSearchTable",prop.getProperty("ErmSearchTable"));
                        dataBinder.putLocal("SearchEngineName",prop.getProperty("SearchEngineName"));
                        dataBinder.putLocal("SearchQueryFormat",prop.getProperty("SearchQueryFormat"));
                        dataBinder.putLocal("ExternalSearchText",prop.getProperty("ExternalSearchText"));
                        dataBinder.putLocal("dSource",prop.getProperty("dSource"));
                        dataBinder.putLocal("AdvSearch",prop.getProperty("AdvSearch"));
                       
                     
            // Write the data binder for the request to stdout
                        serializer.serializeBinder (System.out, dataBinder);
           
            // Send the request to Content Server
                        ServiceResponse response = idcClient.sendRequest(userContext,dataBinder);
           

            // Get the data binder for the response from Server
                        DataBinder responseData = response.getResponseAsBinder();
          

                        DataResultSet resultSet = responseData.getResultSet("SearchResults_Physical");
                        
                        for (DataObject dataObject : resultSet.getRows ()) {

                          System.out.println ("Content ID  is  :  " + dataObject.get ("dDocName"));
            
			// Update logic               
                          DataBinder dataBinder1 = idcClient.createBinder(); 
                          dataBinder1.putLocal("IdcService",prop.getProperty("UpdateIdcService"));
                          dataBinder1.putLocal("dID",dataObject.get ("dID"));
                          dataBinder1.putLocal("dDocName",dataObject.get ("dDocName"));
                          dataBinder1.putLocal("xClassificationGuideRemarks",prop.getProperty("xClassificationGuideRemarks"));
                          dataBinder1.putLocal("xCategoryID",prop.getProperty("xCategoryID"));                          
                          dataBinder1.putLocal("dExtObjectType",prop.getProperty("dExtObjectType"));
                          dataBinder1.putLocal("dMediaType",prop.getProperty("dMediaType"));
            // End                        
                        // Write the data binder for the request to stdout
                        serializer.serializeBinder (System.out, dataBinder1);

                        // Send the request to Content Server
                        ServiceResponse response1 = idcClient.sendRequest(userContext,dataBinder1);
              } 
                  

 
		
		} catch (IdcClientException ice){
			ice.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}

}
