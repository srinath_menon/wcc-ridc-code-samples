This RIDC based Java utility will automate the transfer of custom WSDL from source webcenter content server to target server and also update the end-point url for the wsdl's matching it with the target server. 
The steps to run this utility are :

1. Compile the 3 Java programs which are packaged in the attached ZIP file .

2. Make the corresponding changes to wsdlconfig.properties file as in set the source host name , port , user and similarly for target server . 

3. Run the ExportWSDL class .
