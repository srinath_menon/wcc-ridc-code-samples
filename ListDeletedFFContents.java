import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;

/*
 * @author Srinath Menon
 *
 * This is a class used to search for contents which are deleted from Framework Folder
 * It uses IDC Service GET_DATARESULTSET and OOTB Data Source "Documents" 
 * To get the contents listed which were part of Framework Folder and has been deleted , following parameters are added : whereClause and dStatus
 * resultName - this is the result set which will store the values. It can be name anything . For eg here it is DocList
 */
public class ListDeletedFFContents {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Create a new IdcClientManager
		IdcClientManager manager = new IdcClientManager ();
		try{
			// Create a new IdcClient Connection using idc protocol (i.e. socket connection to Content Server)
			IdcClient idcClient = manager.createClient ("idc://ucmhostname:intradocserverport");

                                 //for using the web connection - start          
                                // IdcClient idcClient = manager.createClient("http://hostname:webport/cs/idcplg");
                                 IdcContext userContext = new IdcContext("weblogic", "<password>");          
                                //for using web connection - end
                                  
                        
			// Create an HdaBinderSerializer; this is not necessary, but it allows us to serialize the request and response data binders
			HdaBinderSerializer serializer = new HdaBinderSerializer ("UTF-8", idcClient.getDataFactory ());
			
			// Databinder for Search request
			DataBinder dataBinder = idcClient.createBinder();
            dataBinder.putLocal("IdcService", "GET_DATARESULTSET");
            dataBinder.putLocal("dataSource","Documents");  // Datasource to list the contents 
            dataBinder.putLocal("resultName","DocList");   //Result set defined . This is user defined and can be set to anything as required .
            dataBinder.putLocal("whereClause","(dProcessingState LIKE 'E' )"); //Key part here . dProcessingState=E corresponds to Framework Folder deleted item. 
			dataBinder.putLocal("dStatus","Expired"); 
            // Write the data binder for the request to stdout
            serializer.serializeBinder (System.out, dataBinder);
            // Send the request to Content Server
            ServiceResponse response = idcClient.sendRequest(userContext,dataBinder);
            // Get the data binder for the response from Content Server
            DataBinder responseData = response.getResponseAsBinder();
            // Write the response data binder to stdout
            serializer.serializeBinder (System.out, responseData);
           
            // Retrieve the ResultSet from the response - this should match the value provided for resultName
            DataResultSet resultSet1 = responseData.getResultSet("DocList");

            // Iterate over the ResultSet, retrieve all the Content ID based on where clause
            for (DataObject dataObject : resultSet1.getRows ()) {

               System.out.println ("Content ID with conversion state as Converted is  : " + dataObject.get ("dDocName") );

             }

			
		} catch (IdcClientException ice){
			ice.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}

}
