import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;
import java.util.*;
import java.text.*;

/*
 * @author Srinath Menon
 * 
 * This is a class used to test the basic functionality of saving a search query using RIDC
 * It is achieved using PNE_SAVE_QUERY service and the corresponding set of parameters associated with it .
 * Parameter topicString1 can be used in different ways where in additional search settings can be set along with the query . 
 */

public class SaveSearchQuery {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

         

   	        // Create a new IdcClientManager
		IdcClientManager manager = new IdcClientManager ();
		try{
			// Create a new IdcClient Connection using idc protocol (i.e. socket connection to Content Server)
			 IdcClient idcClient = manager.createClient ("idc://WCC host or IP:<intradocserverport>");

                                //for using the web connection - start          
                                // IdcClient idcClient = manager.createClient("http://hostname:port/cs/idcplg/");
                                IdcContext userContext = new IdcContext("weblogic", "<pwd>");          
                                //for using web connection - end
                                  
			          
			// Create an HdaBinderSerializer; this is not necessary, but it allows us to serialize the request and response data binders
			HdaBinderSerializer serializer = new HdaBinderSerializer ("UTF-8", idcClient.getDataFactory ());
			
			// Databinder for save query request
			DataBinder dataBinder = idcClient.createBinder();
            dataBinder.putLocal("IdcService", "PNE_SAVE_QUERY");
        /*            
		   topicString1 is the parameter which sets all the values mentioned here for further refining the saved query . Mandatory ones are query title and query text .
           Sample test case here shows one sample for full params and  other one with basic mandatory ones
       
           dataBinder.putLocal("topicString1","addMruRow:pne_portal:SavedQueries:queryTitle,queryText,sortField,sortOrder,resultCount,ftx,SearchQueryFormat,folderChildren");
        */    
            dataBinder.putLocal("topicString1","addMruRow:pne_portal:SavedQueries:queryTitle,queryText");
            
            dataBinder.putLocal("topicString2","updateKeyByName:pne_portal:touchCacheKey:touchCacheKey");
            dataBinder.putLocal("topicsString3","");
            dataBinder.putLocal("numTopics","2");
            dataBinder.putLocal("queryText","dDocTitle <starts> `Test`");
            dataBinder.putLocal("queryTitle","<title>");
            dataBinder.putLocal("Page","HOME_PAGE");
            dataBinder.putLocal("touchCacheKey","");
		/*	The remaining commented parameters if the same are set in topicString1
			
		   dataBinder.putLocal("sortField","dInDate");
           dataBinder.putLocal("sortOrder","Desc");
           dataBinder.putLocal("SearchQueryFormat","Universal");
		   dataBinder.putLocal("resultCount","20");
           dataBinder.putLocal("ftx","");
           dataBinder.putLocal("folderChildren","");
        */   
            serializer.serializeBinder (System.out, dataBinder);
            // Send the request to Content Server
            ServiceResponse response = idcClient.sendRequest(userContext,dataBinder);
            // Get the data binder for the response from Content Server
            DataBinder responseData = response.getResponseAsBinder();
            // Write the response data binder to stdout
            serializer.serializeBinder (System.out, responseData);


 
			
		} catch (IdcClientException ice){
			ice.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}
  }