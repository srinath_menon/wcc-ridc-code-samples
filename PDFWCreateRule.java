import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;
import java.util.*;
import java.text.*;

/*
 * @author Srinath Menon 
 * Sample code for creating PDF Watermark Rules using RIDC
 */
 
public class PDFWCreateRule {
	// RIDC connection information
	private static final String IDC_PROTOCOL = "idc://";
	private static final String RIDC_SERVER = "<ucm server hostname>";
	private static final String RIDC_PORT = "<ucm server intradoc port>";
	private static IdcClientManager m_idcClientManager;
	private static IntradocClient m_idcClient;

	private static final String UTF8 = "UTF-8";

	// Service parameters
	private static final String IDC_SERVICE = "IdcService";
	private static final String SAVE_RULES = "PDFW_SAVE_RULES";
	private static final String GET_RULES = "PDFW_GET_RULES";
	
         // User to execute service calls as
	private static final String USER = "weblogic";

	// Used for getting the Result Set for PDFW Rules - This will be used to update new data
	private static final String RULE_RSET = "PdfwRulesDRS";

        // Used for Saving the New Rules - above one can be re-used
        private static final String IMPORT_RULESET = "PdfwRulesDRS";

        // PdfwRuleDRS result set has 3 parameters which are initialized here 
        public static DataResultSet.Field RULE_NAME= new DataResultSet.Field("pdfwRuleName");
        public static DataResultSet.Field RULE_CRIETERIA= new DataResultSet.Field("pdfwCriteria");
        public static DataResultSet.Field RULE_TEMPLATE= new DataResultSet.Field("pdfwCriteria");

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(final String[] args) {
		try {
			// Get ResultSet PdfwRulesDRS from service call PDFW_GET_RULES
			final DataResultSet templateResultSet = getPdfwRulesDRS();
			// Create New rule based on requirement
			savePdfwRulesDRS(templateResultSet);

		} catch (final IdcClientException ice){
			ice.printStackTrace(System.out);
		}
	}

	/**
	 * This method executes PDFW_GET_RULES and returns the PdfwRulesDRS ResultSet.
	 * @return The PdfwRulesDRS ResultSet.
	 * @throws IdcClientException
	 * @throws IOException
	 */
	private static DataResultSet getPdfwRulesDRS() throws IdcClientException {
		// DataBinder for service call
		DataBinder serviceBinder = getNewDataBinder();

		// Set service parameters - PDFW_GET_RULES
		serviceBinder.putLocal(IDC_SERVICE, GET_RULES);

		// Execute service
		serviceBinder = executeService(serviceBinder, USER);

		// Return PdfwRulesDRS  ResultSet
		return serviceBinder.getResultSet(RULE_RSET);
                

	}

	/**
	 * This method executes PDFW_SAVE_RULES.
	 * @param ResultSet PdfwRulesDRS from service PDFW_SAVE_RULES.
	 * @throws IdcClientException
	 */
	private static void savePdfwRulesDRS(final DataResultSet templateResultSet) throws IdcClientException {
		// DataBinder for service call
		final DataBinder serviceBinder = getNewDataBinder();

		// Set service parameters
		serviceBinder.putLocal(IDC_SERVICE, SAVE_RULES);
                
               /**
                 * Array list initialized to pass the values to 3 parameters in PdfwRuleDRS resultset 
                 * parameters are Rule name , Rule Criteria and Template Content ID attached to Rule 
                 */

                ArrayList<String> rowList = new ArrayList<String>(3);
                rowList.add("<Rule name>"); //eg : RIDC_Rule1
                rowList.add("<Criteria to trigger the rule>"); // eg : dDocType Application
                rowList.add("<content id of the PDFWTemplate which is to be attached with this Rule>");  //This can be checked and used from PDF Watermark Admin Applet
                templateResultSet.addRow(rowList);
				//adding the parameters to PdfwRuleDRS result set
		serviceBinder.addResultSet(RULE_RSET,templateResultSet);

		// Execute service
		executeService(serviceBinder, USER);
	}

	/**
	 * This method executes a service.
	 * @param binder DataBinder containing service parameters.
	 * @param userName User to execute service as.
	 * @return DataBinder from service call.
	 * @throws IdcClientException
	 * @throws IOException
	 */
	public static DataBinder executeService(DataBinder binder, final String userName) throws IdcClientException {
		final HdaBinderSerializer serializer = new HdaBinderSerializer (UTF8, getIntradocClient().getDataFactory ());

		try {
			System.out.println("Service binder before executing:");
			serializer.serializeBinder (System.out, binder);
		} catch (final IOException ioe) {
			ioe.printStackTrace(System.out);
		}

		// Execute the request
	    final ServiceResponse response = getIntradocClient().sendRequest(new IdcContext(userName), binder);
	    System.out.println("Successfully called service");

	    // Get the response as a DataBinder
	    binder = response.getResponseAsBinder();

		try {
			System.out.println("Service binder after executing:");
			serializer.serializeBinder (System.out, binder);
		} catch (final IOException ioe) {
			ioe.printStackTrace(System.out);
		}

	    // Return the response as a DataBinder
		return binder;
	}

	public static IdcClientManager getIdcClientManager() {
		if (m_idcClientManager == null) {
			// Needed to create IntradocClient
			m_idcClientManager = new IdcClientManager();
		}
		return m_idcClientManager;
	}

	public static IntradocClient getIntradocClient() throws IdcClientException {
		if (m_idcClient == null) {
			// Client to talk to WebCenter Content
			m_idcClient = (IntradocClient)getIdcClientManager().createClient(IDC_PROTOCOL + RIDC_SERVER + ":" + RIDC_PORT);
		}
		return m_idcClient;
	}
	/**
	 * This method gets a new DataBinder.
	 * @return A new DataBinder.
	 * @throws IdcClientException
	 */
	public static DataBinder getNewDataBinder() throws IdcClientException {
		return getIntradocClient().createBinder();
	}
}