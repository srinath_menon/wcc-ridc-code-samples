import java.io.*;
import oracle.stellent.ridc.*;
import oracle.stellent.ridc.model.*;
import oracle.stellent.ridc.protocol.*;
import oracle.stellent.ridc.protocol.intradoc.*;
import oracle.stellent.ridc.common.log.*;
import oracle.stellent.ridc.model.serialize.*;
import oracle.stellent.ridc.protocol.http.*;
import java.util.List;
import java.util.*;
import java.text.*;

/*
 * @author Srinath Menon
 * 
 * This code will list all the checked-in content items which are duplicate of the input file . 
 * It can be modified where in the input can be set to CSV file with multiple file items and the output text file will list all the duplicates.
 * It relies on the ESignature component which in turn uses the checksum functionality to do file comparison and list the duplicates.
 * @return ResultSet ESigGlobalValidationResultSet | Information of the matching content item(s)
 */

public class SearchDuplicates {

	/**
	 * @param args
	 */
	public static void main(String[] args) {


               InputStream fileStream = null;           
 

   	    // Create a new IdcClientManager
		IdcClientManager manager = new IdcClientManager ();
		try{
			// Create a new IdcClient Connection using idc protocol (i.e. socket connection to Content Server)
			IdcClient idcClient = manager.createClient ("idc://<ucmhostname>:intradocserverport");

                                 //  for using the web connection - start          
                                 //  IdcClient idcClient = manager.createClient("http://<ucmhostname>:<webport>/cs/idcplg/");
                                         
                                 //Initialize the user 
								 IdcContext userContext = new IdcContext("<username>", "<password>");          
                                  
                        
			// Create an HdaBinderSerializer; this is not necessary, but it allows us to serialize the request and response data binders
			HdaBinderSerializer serializer = new HdaBinderSerializer ("UTF-8", idcClient.getDataFactory ());
		
                        //The IdcService ESIG_VALIDATE_FILE_GLOBAL needs to pass the file content as part of request to validate
                        String filename = "<path to the file to be checked with>";
                        fileStream = new FileInputStream(filename);
                        long fileLength = new File(filename).length();
 

	
			// Issuing the service call
			            DataBinder dataBinder = idcClient.createBinder();
                        dataBinder.putLocal("IdcService", "ESIG_VALIDATE_FILE_GLOBAL");
                        dataBinder.putLocal("ESigContext","global"); //to indicate the search is performed on all items in system.
                        dataBinder.addFile("ESigValidationFile" ,new TransferFile(fileStream, filename, fileLength, "text/html"));
                    
           
            // Send the request to Content Server
                        ServiceResponse response = idcClient.sendRequest(userContext,dataBinder);
           

            // Get the data binder for the response from Content Server
                        DataBinder responseData = response.getResponseAsBinder();
          
            // Result set 
                        DataResultSet resultSet = responseData.getResultSet("ESigGlobalValidationResultSet");


            //Export the list to a file 
                        PrintStream out = new PrintStream(new FileOutputStream("DuplicateFiles.txt"));     
 
            // Iterate over the ResultSet, retrieve properties from the content items

                        for (DataObject dataObject : resultSet.getRows ()) {

                          System.setOut(out);
                    
                          System.out.println ("Content ID :" + dataObject.get ("dDocName") + "\t" + "Revision : " + dataObject.get ("dRevLabel") + "\t" + "Title of content item : " + dataObject.get ("dDocTitle")) ;


                        }
 
		
		} catch (IdcClientException ice){
			ice.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}

}
